package com.java110.intf.inner;

public interface IGatewayInitInnerService {

    public void init();

    void subscribeMqtt();
}
